using System.Collections.Generic;
using System.Linq;
using UIKit;

namespace Extension
{
    public class AccordFinder : IAccordFinder
    {
        private readonly List<string> _resourcesNames;

        public AccordFinder()
        {
            this._resourcesNames = new List<string>
            {
                "cM",
                "dm1",
                "dm2",
                "dm3",
                "gM1",
                "gM2"
            };
        }

		public IEnumerable<UIImageView> FindAccords(string query)
        {
			var resources = new List<string>();
			if (string.IsNullOrEmpty(query))
			{
				resources = _resourcesNames;
			}
			else
			{
				foreach (var resource in _resourcesNames)
				{
					if (resource.ToLower().Contains(query.ToLower()))
					{
						resources.Add(resource);
					}
				}
			}
            var images = 
				resources.Select(resource => new UIImageView(UIImage.FromBundle($"{resource}.png"))).ToList();
			return images;
        }

        public int Length()
        {
            return _resourcesNames.Count();
        }
    }
}