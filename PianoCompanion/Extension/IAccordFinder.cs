﻿using System.Collections.Generic;
using UIKit;

namespace Extension
{
	public interface IAccordFinder
	{
		IEnumerable<UIImageView> FindAccords(string query);
		int Length();
	}
}
